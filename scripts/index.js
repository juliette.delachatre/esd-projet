if (document.getElementById('boutton')){
  document.getElementById('boutton').onclick = click;
}
let score = 0;
function click() {
  let radios = document.getElementsByClassName('sectionQuizz__input');
  let j = 0;
  for (let i = 0, length = radios.length; i < length; i++) {
    if (radios[i].checked) {
        j++;
        if (radios[i].value == "reponse-1-3" || radios[i].value == "reponse-2-3" || radios[i].value == "reponse-3-2" || radios[i].value == "reponse-4-3" || radios[i].value == "reponse-5-1") {
          score++;
        }
    }
  }
  if(j !== 5) {
      alert('Merci de répondre à toutes les questions !');
      return false;
    }
    if (score !== 5) {
      alert('Tu as ' + score + ' bonne(s) réponse(s) sur 5, essaye encore !');
      score = 0;
      return false;
    }
}
